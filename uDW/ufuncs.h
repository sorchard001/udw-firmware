/*
   DriveWire server implementation for uDW
   - general purpose functions, stdlib replacements etc

   Copyright 2014-2016 Tormod Volden

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

int ustrncmp(const char *s1, const char *s2, int n);
int ustrcmp(const char *s1, char *s2);
int ustrcpy(char *d, const char *s);
unsigned int ustrlen(char *s);
int uisdigit(const char c);
int uscanf2d(const char *start, int *value);
#ifdef UDWDEBUG
void debug_print_2x(int num);
void debug_print_4x(int num);
void debug_print_value(int num);
#endif

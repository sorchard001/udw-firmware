/*
   DriveWire server implementation for uDW
   - relatively hardware independent part

   Copyright 2014-2016 Tormod Volden

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stm32f10x.h>
#include <stm32f10x_rcc.h>
#include <misc.h>
#include <uart.h>
#include <ff.h>
#include "rtc_32f1.h"
#include "rtc_udw.h"
#include "ufuncs.h"
#include "udw.h"
#include "udw_debug.h"
#include "dw.h"
#include "vchan.h"
#include "tapeout.h"

#define MAX_DRIVES 16
/* the first are fixed ones, DRIVEx */
#define FIXED_DRIVES 8
/* the remaining ones are for OP_NAMEOBJ_MOUNT */

/* DriveWire protocol */
#define SECTOR 256
#define E_CRC 0xF3
#define E_READ 0xF4
#define E_WRITE 0xF5
#define E_NREADY 0xF6

/* from main.c */
extern __IO uint32_t TimingDelay ;
extern __IO uint32_t Timer ;
extern __IO int breathing_led;
void Delay( uint32_t nTime );

/* shared with debug routines */
USART_TypeDef *active_uart;
int debug_cursor_enabled = 0;
FIL Fil;
char Buff[1024];
int debug_stat = 0;
int startuptime, firstreqtime = 0;
int opentime, seektime, readtime, writetime;

/* private */
//static FIL *fd[MAX_DRIVES] = {0};
static char fname[MAX_DRIVES][32];
static FATFS Fatfs;
static int manual_led;
static int rtc_ok;

/* private prototypes */
static void dw_nameobj_mount(int);
static void dw_write_op();
static void dw_readex_op();
static void dw_getstat_op();
static void dw_time_op();
static void dw_init_op();
static void fill_slots(void);

int read_io(int *in)
{
	int recv = uart_getc_timeout(active_uart, READ_TIMEOUT);
	if (recv < 0)
		return 1;
	*in = recv;
	return 0;
}

int send_io(int out)
{
	return uart_putc(out, active_uart);
}

int dw_server(USART_TypeDef *uart_in)
{
	active_uart = uart_in;

	rtc_ok = setup_time(0);

	f_mount(0, &Fatfs);
	
	/* get filenames for prefilled slots */
	fill_slots();

	/* set IO_UART baud rate based on "flag" files */
	{
		FILINFO fno;
		int baudRate;

		if (f_stat("COCO3", &fno) == FR_OK)
			baudRate = BAUD_COCO3;
		else if (f_stat("COCO1", &fno) == FR_OK)
			baudRate = BAUD_COCO1;
		else
			baudRate = BAUD_DEFAULT;
		uart_open(IO_UART, baudRate, 0);
	}

	startuptime = 0xFFFF - Timer;
	breathing_led = 1;

	int oldJackDetect = 0, newJackDetect;

	while (1) {
		int in = 0;
		int timeout;
		static int debug_print_cursor = 1;

		newJackDetect = udwJackDetect();
		if(newJackDetect != oldJackDetect) {
			if(newJackDetect) {
				tapeoutPlayBoot();
				oldJackDetect = 1;
			} else {
				Delay(300);	// debounce
				oldJackDetect = udwJackDetect();
			}
		}

		if (!manual_led) {
			setRed(0);
			breathing_led = 1;
		}
		if (debug_cursor_enabled) {
			if (debug_print_cursor)
				debug_print_char('_');
			else
				debug_print_char(' ');
		}
		timeout = read_io(&in);
		if (debug_cursor_enabled) {
			debug_print_char('\b');
		}
		if (timeout) {
			debug_print_cursor = 1 - debug_print_cursor;
			continue;
		} else {
			debug_print_cursor = 1;
		}

		if (!manual_led) {
			breathing_led = 0;
			setGreen(0);
			setRed(10);
		}

		switch (in) {
			case 0x01 :	/*  OP_NAMEOBJ_MOUNT 0x01 */
				if (!firstreqtime)
					firstreqtime = 0xFFFF - Timer;
				dw_nameobj_mount(0);
				break;
			case 0x02 :	/* OP_NAMEOBJ_CREATE 0x02 */
				dw_nameobj_mount(1);
				break;
			case 0x57 :	/* OP_WRITE 0x57 */
				dw_write_op();
				break;
			case 0x77 :	/* OP_REWRITE 0x77 */
				dw_write_op();
				debug_print_char('r');
				break;
			case 0xD2 :	/* OP_READEX 0xD2 */
				dw_readex_op();
				break;
			case 0xF2 :	/* OP_REREADEX 0xF2 */
				dw_readex_op();
				debug_print_char('r');
				break;
			case 'G' :	/* OP_GETSTAT */
			case 'S' :	/* OP_SETSTAT */
				dw_getstat_op();
				break;
			case '#' :	/* OP_TIME 0x23 */
				dw_time_op();
				break;
			case 'Z' :	/* OP_DWINIT 0x5A */
				dw_init_op();
				break;
			case 'I' :	/* OP_INIT 0x49 */
				/* ignore */
				break;
			case 0x45 :	/* OP_SERINIT */
				dw_serinit_op();
				break;
			case 0xC5 :	/* OP_SERTERM */
				dw_serterm_op();
				break;
			case 0xC4 :	/* OP_SERSETSTAT */
				dw_sersetstat_op();
				break;
			case 0x43 :	/* OP_SERREAD */
				dw_serread_op();
				break;
			case 0x63 :	/* OP_SERREADM */
				dw_serreadm_op();
				break;
			case 0xC3 :	/* OP_SERWRITE */
				{
				int chan;
				read_io(&chan);
				dw_serwrite(chan);
				}
				break;
			case 0x64 :	/* OP_SERWRITEM */
				dw_serwritem_op();
				break;
			default :
				if (in >= 0x80 && in < 0x8F) {	/* OP_FASTWRITE */
					dw_serwrite(in - 0x80);
					break;
				}
#ifdef UDWDEBUG
				/* check for debug commands on debug port */
				if (active_uart == DEBUG_UART)
					debugInput(in);
#endif
		} /* switch */
	} /* while */
}

static int sprint6847(char *dst, const char *src)
{
	char c;
	char *o = dst;

	while ((c = *src++)) {
		if (c < 32)
			c += 128;
		else if (c < 64)
			c += 64;
		else if (c >= 96)
			c -= 96;
		*dst++ = c;
	}
	return dst - o;
}

#define DECB_PAYLOAD ((unsigned char *) &Buff[5])

/* for loading directly into VDU memory */
static void decb_vdu(unsigned char *c)
{
	int len;

	len = c - DECB_PAYLOAD;
	if (len + 10 > sizeof(Buff))
		debug_print(" Buff overflow! ");
	Buff[0] = 0x00;
	Buff[1] = len >> 8;
	Buff[2] = len & 0xFF;
	/* default VDU memory location */
	Buff[3] = 0x04;
	Buff[4] = 0x40; /* 3rd line because the prompt scrolls screen */
	/* return non-executable $FFxx address in footer */
	*c++ = 0xFF; *c++ = 0x00; *c++ = 0x00; *c++ = 0xFF; *c++ = 0xFF;
	return;
}

static int blank_eol(unsigned char *c)
{
	int n;

	for (n = 0; (c - DECB_PAYLOAD) % 32 != 0; n++)
		*c++ = ' ' + 64;
	return n;
}

/* FIXME: Make a dw_reply_add() that can be called repeatedly? */

static void dw_prepare_reply(const unsigned char *message)
{
	unsigned char *c = DECB_PAYLOAD;

	c += sprint6847(c, message);
	c += blank_eol(c);
	decb_vdu(c);
	return;
}

static void dw_prepare_info(void)
{
	unsigned char *c = DECB_PAYLOAD;

	c += sprint6847(c, "UDW" UDWVERSION_STRING );
	if (udwCardDetect())
		c += sprint6847(c, " (SD CARD PRESENT)" );
	else
		c += sprint6847(c, " (SD CARD SLOT EMPTY)" );

	if (rtc_ok)
		c += sprint6847(c, " (RTC OK)" );
	else
		c += sprint6847(c, " (RTC FAIL)" );

	c += blank_eol(c);
	decb_vdu(c);
	return;
}

static void dw_prepare_vc(void)
{
	unsigned char *c = DECB_PAYLOAD;
	int vc;

	c += sprint6847(c, "VC CHANNELS " );
	for (vc = 0; vc < VIRTCHANS; vc++) {
		int numb;

		numb = vc_in_waiting(vc);
		*c++ = '0' + 64 + vc;
		*c++ = ':' + 64;
		*c++ = '0' + 64 + numb / 10;
		*c++ = '0' + 64 + numb % 10;
		*c++ = ' ' + 64;
	}
	c += blank_eol(c);
	decb_vdu(c);
	return;
}

static void dw_prepare_drivelist(void)
{
	unsigned char *c = DECB_PAYLOAD;
	int i;

	c += sprint6847(c, "FIXED DRIVES: ");
	for (i = 0; i < FIXED_DRIVES; i++) {
		/* FIXME if i > 9 */
		*c++ = i + '0' + 64;
		*c++ = '=' + 64;
		c += sprint6847(c, fname[i]);
		*c++ = ' ' + 64;
	}
	c += blank_eol(c);
	decb_vdu(c);
	return;
}

int list_dir(char *dirname)
{
	DIR dir;
	FILINFO fno;
	FRESULT rc;
	unsigned char *c = DECB_PAYLOAD;
	char *i;
	char *lastdir = dirname;
	char *wildname = 0;
	int wildlen = 0;
	int skipslash = 0;
	char emptydir[] = "";
	unsigned char *top;
	int page = 0;

	/* simple support for trailing '*' wildcard on filenames */
	for (i = dirname; *i; i++) {
		if (*i == '/') {
			lastdir = i;
			skipslash = 1;
		} else if (*i == '*') {
			*i = '\0';
			if (skipslash)
				*lastdir = '\0';
			else
				dirname = emptydir;
			wildname = lastdir + skipslash;
			wildlen = i - wildname;
			break;
		} else if (*i == '#') {
			*i = '\0';
			page = *(i+1) - '0';
			break;
		}
	}
	rc = f_opendir(&dir, dirname);
	if (rc != FR_OK) {
		debug_print("bad directory\n");
		return 1;
	}
	top = c;
	for (;;) {
		rc = f_readdir(&dir, &fno);
		if (rc != FR_OK || fno.fname[0] == 0)
			break;
		if (wildlen && ustrncmp(wildname, fno.fname, wildlen))
			continue;
		/* will fill up screen? assuming all characters printable */
		if (c - top + ustrlen(fno.fname) + 2 > 512-96) {
			*c++ = '#' + 64;
			if (--page < 0)
				break;
			else
				c = top;
		}
		c += sprint6847(c, fno.fname);
		debug_print(fno.fname);
		if (fno.fattrib & AM_DIR) {
			*c++ = '/' + 64; // c += sprint6847("/");
			debug_print("/ ");
		} else {
			debug_print("  ");
		}
		*c++ = ' ' + 64; /* space on 6847 */
		// debug_print("\n");
	}
	/* always return something (or DWLOAD will crap out) */
	if (c == top)
		c += sprint6847(c, "(no files)");
	c += blank_eol(c);
	decb_vdu(c);
	return 0;
}

/* Magic ! file names for uDW commands */
/* returns 0 if no reply buffer has been prepared and
   we have already sent a "mount error" */
static int udw_command(char *cmd)
{
	/* some commands must return "mount error" to client so that
	   it doesn't start asking for sector blocks */
	if (!ustrcmp(cmd, "DFU")) {
		send_io(0); // reply to client before we go poof
		debug_print("resetting... \n");
		udwResetToDFU();
		return 0; /* not reached */
	} else if (!ustrcmp(cmd, "DEBUG")) {
		send_io(0); // reply to client while we can
		active_uart = DEBUG_UART; // switch to debug port
		debug_cursor_enabled = 1;
		debug_print("entering debug mode\n");
		return 0;
	} else if (!ustrcmp(cmd, "STAT")) {
		debug_stat = 1 - debug_stat;
		debug_print("stat=");
		debug_print_2x(debug_stat);
		dw_prepare_reply(debug_stat?"STAT ON ":"STAT OFF ");
	} else if (!ustrncmp(cmd, "LED", 3)) {
		int red, green, pos;
		pos = uscanf2d(cmd + 3, &red);
		if (pos) {
			manual_led = 1;
			breathing_led = 0;
			setRed(red*2);
			pos = uscanf2d(cmd + 3 + pos, &green);
			if (pos) {
				setGreen(green*2);
			} else {
				setGreen(0);
			}
		} else {
			manual_led = 0;
			setRed(0);
			setGreen(0);
			breathing_led = 1;
		}
		dw_prepare_reply("SET LED ");
	} else if (!ustrncmp(cmd, "SND", 3)) {
		send_io(0); // reply to client because we will be busy
		if (cmd[3])
			hf_test(10000);
		else
			snd_test(10000);
		debug_print("snd ");
		return 0;
	} else if (!ustrncmp(cmd, "SD", 2)) {
		if (cmd[2] == '1') {
			powerUpSD();
			setRed(0);
			breathing_led = 1;
			manual_led = 0;
		} else if (cmd[2] == '0') {
			powerDownSD();
			manual_led = 1;
			breathing_led = 0;
			setRed(255);
		} else {
			powerDownSD();
			breathing_led = 0;
			setRed(255);
			Delay(200);
			powerUpSD();
			setRed(0);
			breathing_led = 1;
		}
		debug_print("sd ");
		dw_prepare_reply("SD POWER SET ");
	} else if (!ustrncmp(cmd, "WIFI", 4)) {
		if (cmd[4] == '1') {
			powerWIFI(1);
			dw_prepare_reply("WIFI POWER ON ");
		} else {
			powerWIFI(0);
			dw_prepare_reply("WIFI POWER OFF ");
		}
		debug_print("wifi ");
	} else if (!ustrcmp(cmd, "VC")) {
		dw_prepare_vc();
	} else if (!ustrcmp(cmd, "RTC")) {
		rtc_ok = setup_time(1);
		debug_print("rtc reset ");
		dw_prepare_reply("RTC RESET ");
	} else if (!ustrncmp(cmd, "TIME", 4)) {
		settime(cmd + 4);
		debug_print("!time ");
		dw_prepare_reply("TIME SET ");
	} else if (!ustrncmp(cmd, "DATE", 4)) {
		setdate(cmd + 4);
		debug_print("!date ");
		dw_prepare_reply("DATE SET ");
	} else if (!ustrcmp(cmd, "INFO")) {
		debug_print("!info ");
		dw_prepare_info();
	} else if (!ustrncmp(cmd, "DRIVE", 5)) {
		int drive;
		int pos;
		pos = uscanf2d(cmd + 5, &drive);
		if (pos) {
			while (cmd[5 + pos] == ' ')
				pos++;
			ustrcpy(fname[drive], cmd + 5 + pos);
			debug_print("!drive ");
			debug_print_2x(drive);
			debug_print(fname[drive]);
		}
		dw_prepare_drivelist();
	} else if (!ustrncmp(cmd, "LS", 2)) {
		char *dirname = cmd + 2;
		/* skip optional space */
		if (*dirname == ' ')
			dirname++;
		/* fill buffer with directory listing */
		if (list_dir(dirname))
			dw_prepare_reply("bad dir");
		debug_print(" listed\n");
	} else {
		/* default for empty/unknown ! command */
		/* fill buffer with current directory listing */
		list_dir("");
		debug_print(" listed\n");
	}

	return 1; /* we have filled buffer with something */
}

/* on OP_NAMEOBJ_MOUNT 0x01 */
	// receive object name (length first)
	// check if already in slot and open
	// check file exist (and open?)
	// ? find free drive slot and store name and fp
	// send drive slot number
static void dw_nameobj_mount(int create)
{
	static int named_slot = MAX_DRIVES - 1;
	int slot = 0;
	int namelen;
	int i;
	int exists;
	char objname[32];
	
	if (read_io(&namelen) || !namelen) {
		debug_print("OP_NAMEOBJ_MOUNT without name?\n");
		goto reply_slot;
	}
	if (namelen>31)
		namelen = 31;
	for (i=0; i<namelen; i++) {
		int letter;
		if (read_io(&letter) || !letter) {
			debug_print("OP_NAMEOBJ_MOUNT short name?\n");
			goto reply_slot;
		}
		objname[i] = (char) letter;
	}
	objname[i] = '\0';

	/* FIXME: Check for ! command first, use magic slot e.g. 255
	   and objname instead of copying command to fname[] */

	/* assign next slot, counting downwards */
	slot = named_slot;
	named_slot--;
	if (named_slot == FIXED_DRIVES)
		named_slot = MAX_DRIVES - 1;

	/* name table starts with slot 0 */
	ustrcpy(fname[slot], objname);

/*
	if (fd[slot] && ustrcmp(objname, fname[slot])) {
		f_close(fd[slot]);
		fd[slot] = 0;
		ustrcpy(fname[slot], objname);
	}
*/

	/* give client time to start listening */
	Delay(1);

	if (objname[0] == '!') {
		if (!udw_command(objname + 1))
			slot = -1; /* reply already sent */
	} else {
		/* Check for existence of normal file */
		FILINFO fno;

		exists = (f_stat(objname, &fno) == FR_OK);
		if (exists && (fno.fattrib & AM_DIR)) {
			debug_print("error: is dir\n");
			slot = 0;
		}
		if ((!create && !exists) || (create && exists))
			slot = 0;

		if (create) {
			FIL cfd;
			FRESULT rc;

			rc = f_open(&cfd, objname, FA_CREATE_NEW);
			if (rc != FR_OK) {
				debug_print("file creation failed: ");
				debug_print_2x(rc & 0xFF);
				slot = 0;
			}
			f_close(&cfd);
		}
	}

reply_slot:
	/* is this delay necessary? */
	Delay(10);
	if (slot != -1)
		send_io((char) slot);

	if (!slot) {
		/* why this delay? */
		Delay(10);
		debug_print("mount failed: ");
		debug_print(objname);
		debug_print_char('\n');
	}
}

/* on OP_WRITE */
/* will write to slot dnum, open file from fname[] if needed */
static void dw_write_op()
{
	FRESULT rc;
	int error_condition = 0;
	unsigned int checksum;
	unsigned int chk1 = 0, chk2 = 0;
	int dnum, lsn0, lsn1, lsn2, lsn;
	int bytesRead = 0;
	int i;
	FIL cfd;

	// receive 1 byte drive number
	// receive 3 bytes LSN
	// receive sector data (and calculate checksum)
	// receive 2 checksum bytes
	// locate disk image name and fp in drive slots
	// open disk image file (if not open)
	// seek to LSN
	// write buffer to sector
	// ? send Write Failure with error code if not successful
	// ? send zero byte (success)

	if (read_io(&dnum) ||
	    read_io(&lsn0) ||
	    read_io(&lsn1) ||
	    read_io(&lsn2)) {
		debug_print("timeout on write packet header\n");
		goto write_reply;
		// return 1;
	}

	checksum = 0;
	for (i = 0; i < SECTOR; i++) {
		int data;
		if (read_io(&data)) {
			error_condition = E_WRITE;
			debug_print("timeout on write packet data\n");
			goto write_reply;
			// return 1;
		}
		Buff[i] = data;
		checksum += data;
	}
	checksum &= 0xFFFF;

	if (read_io(&chk1) || read_io(&chk2)) {
		error_condition = E_WRITE;
		debug_print("timeout on write packet checksum\n");
		goto write_reply;
		// return 1;
	}
	if ((checksum >> 8) != chk1 || (checksum & 0xFF) != chk2) {
		error_condition = E_CRC;
		debug_print("bad checksum\n");
		goto write_reply;
	}

	lsn = (lsn0 << 16) | (lsn1 << 8) | lsn2;
		
	if (dnum < 0 || dnum > MAX_DRIVES) {
		debug_print_value(dnum);
		debug_print(" for drive number\n");
		error_condition = E_NREADY;
		goto write_reply;
	}
	if (!fname[dnum][0] || fname[dnum][0]=='!') {
		/* no file in drive slot */
		error_condition = E_NREADY;
		goto write_reply;
	}

	Timer = 0xFFFF;
	rc = f_open(&cfd, fname[dnum], FA_WRITE | FA_OPEN_ALWAYS);
	opentime = 0xFFFF - Timer;

	if (rc != FR_OK) {
			debug_print("Could not open ");
			debug_print(fname[dnum]);
			debug_print("\n");
			error_condition = E_NREADY;
			goto close_reply;
	}

	/* check against a maximum size instead? */
	if (lsn * SECTOR > cfd.fsize) {
			debug_print("expanding file");
			debug_print_4x(lsn >> 16);
			debug_print_4x(lsn & 0xFFFF);
			debug_print_char('\n');
	}

	rc = f_lseek(&cfd, lsn * SECTOR);
	/* TODO: check return value too */
	seektime = 0xFFFF - Timer;
	rc = f_write(&cfd, Buff, SECTOR, &bytesRead);
	writetime = 0xFFFF - Timer;
	if (rc != FR_OK) {
		debug_print("f_write failed: ");
		debug_print_2x(rc & 0xFF);
		error_condition = E_WRITE;
		goto close_reply;
	}
	if (bytesRead != SECTOR) {
		debug_print("f_write wrote fewer bytes");
		error_condition = E_WRITE;
		goto close_reply;
	}

close_reply:
	/* close at each time */
	f_close(&cfd);

write_reply:
	/* TODO: Delay before sending reply? */
	Delay(1);
	send_io(error_condition);
}

/* on OP_READEX 0xD2 */
/* will read from slot dnum, open file from fname[] if needed */
static void dw_readex_op()
{
	FRESULT rc;
	int error_condition = 0;
	unsigned int checksum;
	unsigned int chk1 = 0, chk2 = 0;
	int dnum, lsn0, lsn1, lsn2, lsn;
	int bytesRead = 0;
	int i;
	FIL cfd;
	int opened = 0;
	int buff_offset = 0;

	// receive 1 byte drive number
	// receive 3 bytes LSN
	// locate disk image name and fp in drive slots
	// open disk image file (if not open)
	// seek to LSN
	// read sector to buffer (and calculate checksum)
	// ? send Read Failure with error code if not successful
	// ? send zero byte (success)
	// ? send checksum bytes
	// send sector

	// receive checksum bytes
	// send match result (E_CRC or zero)

	if (read_io(&dnum) ||
	    read_io(&lsn0) ||
	    read_io(&lsn1) ||
	    read_io(&lsn2)) {
		debug_print("timeout on readex packet\n");
		return;
	}

	lsn = (lsn0 << 16) | (lsn1 << 8) | lsn2;

	/* FIXME: Check for magic ! command slot and act on it first */

	if (dnum < 0 || dnum > MAX_DRIVES) {
		debug_print_value(dnum);
		debug_print(" for drive number\n");
		error_condition = E_NREADY;
		goto fill_buffer;
	}

	if (!fname[dnum][0]) {
		// nothing in drive slot 
		error_condition = E_NREADY;
		goto fill_buffer;
	}
	/* let's hope buffer was filled out by ! mount request just before */
	/* FIXME: check if buffer was overwritten by other request */
	if (fname[dnum][0] == '!') {
		Delay(1); /* otherwise too quickly */
		/* buffer was prepared by OP_NAMEOBJ_MOUNT request */
		/* support multiple packages */
		buff_offset = SECTOR * lsn;
		/* write out whole sector for now */
		bytesRead = SECTOR;
		goto send_buffer;
	}

	Timer = 0xFFFF;
	rc = f_open(&cfd, fname[dnum], FA_READ);
	opentime = 0xFFFF - Timer;

	if (rc != FR_OK) {
			debug_print("Could not open ");
			debug_print(fname[dnum]);
			debug_print("\n");
			error_condition = E_NREADY;
			goto fill_buffer;
	}
	opened = 1;

	if (lsn * SECTOR > cfd.fsize) {
			debug_print("lsn too large for file size");
			debug_print_4x(lsn >> 16);
			debug_print_4x(lsn & 0xFFFF);
			debug_print_char('\n');
			error_condition = E_READ;
			goto fill_buffer;
	}

fill_buffer:
	if (error_condition) {
		for (i=0; i<SECTOR; i++)
			Buff[i] = '\0';
	} else {
		rc = f_lseek(&cfd, lsn * SECTOR);
		seektime = 0xFFFF - Timer;
		rc = f_read(&cfd, Buff, SECTOR, &bytesRead);
		readtime = 0xFFFF - Timer;
		/* TODO: check return value too */
	}

send_buffer:
	/* TODO: Delay before sending? */
	checksum = 0;
	for (i=0; i<bytesRead; i++) {
		send_io(Buff[i + buff_offset]);
		checksum += (unsigned char) Buff[i + buff_offset];
	}
	/* pad sector with zero bytes */
	for (i=bytesRead; i<SECTOR; i++) {
		send_io('\0');
	}

	checksum &= 0xFFFF;
	if (read_io(&chk1) || read_io(&chk2)) {
		debug_print("timeout on checksum\n");
	}
	if ((checksum >> 8) != chk1 || (checksum & 0xFF) != chk2) {
		error_condition = E_CRC;
		debug_print("bad checksum\n");
	}

	/* close at each time */
	if (opened)
		f_close(&cfd);

	Delay(1);
	send_io(error_condition);

#ifdef UDWDEBUG
	debugStats(error_condition, lsn);
#endif
}

static void dw_init_op()
{
	int driver;
	Delay(1);
	read_io(&driver);
}

static void dw_getstat_op()
{
	int drive;
	int status;
	read_io(&drive);
	read_io(&status);
}

static void dw_time_op()
{
	RTCTIME rtc;

	Delay(1);
	rtc_gettime (&rtc);
	send_io(rtc.year - 1900);
	send_io(rtc.month);
	send_io(rtc.mday);
	send_io(rtc.hour);
	send_io(rtc.min);
	send_io(rtc.sec);
	debug_print_char('T');
}

static void fill_slots(void)
{
	int i;

	/* TODO: read from configuration file */

	/* associate drive number with file name */
	/* fill in DRIVE0 DRIVE1 DRIVE2 DRIVE3 ... */
	for (i = 0; i < FIXED_DRIVES; i++) {
		ustrcpy(fname[i], "DRIVE");
		fname[i][5] = '0' + i;
		fname[i][6] = '\0';
	}
	/* blank out remaining drives */
	for (i = FIXED_DRIVES; i < MAX_DRIVES; i++)
		fname[i][0] = '\0';
}

#include <stm32f10x.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_spi.h>
#include "spi.h"

static const uint16_t speeds[] = {
  [SPI_SLOW] = SPI_BaudRatePrescaler_64,
  [SPI_MEDIUM] = SPI_BaudRatePrescaler_8,
  [SPI_FAST] = SPI_BaudRatePrescaler_2};

void spiInit(SPI_TypeDef *SPIx)
{
  SPI_InitTypeDef SPI_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  uint32_t RCC_APB2Periph_GPIOx;
  GPIO_TypeDef* GPIOx;
  uint16_t SCK_Pin, MISO_Pin, MOSI_Pin;

  GPIO_StructInit(&GPIO_InitStructure);
  SPI_StructInit(&SPI_InitStructure);

  if (SPIx == SPI2) {  
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_SPI2, ENABLE );
        RCC_APB2Periph_GPIOx = RCC_APB2Periph_GPIOB;
	GPIOx = GPIOB;
	SCK_Pin = GPIO_Pin_13;
	MISO_Pin = GPIO_Pin_14;
	MOSI_Pin = GPIO_Pin_15;
  } else if (SPIx == SPI1) {
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_SPI1, ENABLE );
        RCC_APB2Periph_GPIOx = RCC_APB2Periph_GPIOA;
	GPIOx = GPIOA;
	SCK_Pin = GPIO_Pin_5;
	MISO_Pin = GPIO_Pin_6;
	MOSI_Pin = GPIO_Pin_7;
  }  else  {  //  other SPI devices --
      /* SPI only implemented on SPI1 and SPI2 */
     return;
  }

  /* Enable clocks, configure pins */
  RCC_APB2PeriphClockCmd ( RCC_APB2Periph_GPIOx, ENABLE );

  /* SCK */
  GPIO_StructInit (&GPIO_InitStructure );
  GPIO_InitStructure.GPIO_Pin = SCK_Pin ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz ;
  GPIO_Init (GPIOx, &GPIO_InitStructure );

  /* MISO */
  GPIO_StructInit (&GPIO_InitStructure );
  GPIO_InitStructure.GPIO_Pin = MISO_Pin ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz ;
  GPIO_Init (GPIOx, &GPIO_InitStructure );

  /* MOSI */
  GPIO_StructInit (&GPIO_InitStructure );
  GPIO_InitStructure.GPIO_Pin = MOSI_Pin ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz ;
  GPIO_Init (GPIOx, &GPIO_InitStructure );

  SPI_StructInit(&SPI_InitStructure);
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low; 
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = speeds[SPI_MEDIUM];
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(SPIx, &SPI_InitStructure);

  SPI_Cmd(SPIx, ENABLE);   
}

void spiDeInit(SPI_TypeDef *SPIx)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_TypeDef* GPIOx;
  uint16_t SCK_Pin, MISO_Pin, MOSI_Pin;

  SPI_Cmd(SPIx, DISABLE);   

  if (SPIx == SPI2) {  
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_SPI2, DISABLE ); /* needed? */
	GPIOx = GPIOB;
	SCK_Pin = GPIO_Pin_13;
	MISO_Pin = GPIO_Pin_14;
	MOSI_Pin = GPIO_Pin_15;
  } else if (SPIx == SPI1) {
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_SPI1, DISABLE ); /* needed? */
	GPIOx = GPIOA;
	SCK_Pin = GPIO_Pin_5;
	MISO_Pin = GPIO_Pin_6;
	MOSI_Pin = GPIO_Pin_7;
  }  else  {  //  other SPI devices --
      /* SPI only implemented on SPI1 and SPI2 */
     return;
  }
  /* make pins input, weak pull-down needed? */
  GPIO_StructInit (&GPIO_InitStructure );
  GPIO_InitStructure.GPIO_Pin = MISO_Pin | MOSI_Pin | SCK_Pin ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz ;
  GPIO_Init (GPIOx, &GPIO_InitStructure );
}

int spiReadWrite(SPI_TypeDef* SPIx, uint8_t *rbuf, 
         const uint8_t *tbuf, int cnt, enum spiSpeed speed)
{
  int i;

  SPIx->CR1 = (SPIx->CR1 & ~SPI_BaudRatePrescaler_256) | 
               speeds[speed];

  for (i = 0; i < cnt; i++){
    if (tbuf) {
      SPI_I2S_SendData(SPIx, *tbuf++);
    } else {
      SPI_I2S_SendData(SPIx, 0xff);
    }
    while (SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_RXNE) == RESET);
    if (rbuf) {
      *rbuf++ = SPI_I2S_ReceiveData(SPIx);
    } else {
      SPI_I2S_ReceiveData(SPIx);
    }
  }
  return i;
}

int spiReadWrite16(SPI_TypeDef* SPIx, uint16_t *rbuf, 
     const uint16_t *tbuf, int cnt, enum spiSpeed speed)
{
  // unimplemented !
  return 0;
}




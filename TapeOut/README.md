## uDW Tape Output

Modified version of Tormod Volden's uDW (DriveWire microserver) firmware, adding a cassette audio output for the purpose of conveniently bootstrapping a driver into machines that don't have a DWLOAD ROM.

The idea is to type CLOADM<ENTER> on the Dragon, then plug the Dragon cassette jack into the audio out socket on the uDW. The uDW detects this and converts and plays the bootstrap file TAPEBOOT.BIN from the SD card.

The TAPEBOOT.BIN included here is a RAM-based DWLOAD driver and is simply a renamed copy of Tormod's DWLOADR.BIN and works in the same way.

e.g. after loading type EXEC&H7400"MYFILE" or just EXEC to load and run AUTOLOAD.DWL from the SD card.

Alternatively replace TAPEBOOT.BIN with any DragonDOS binary to do something different.

Also included is a new firmware binary uDW.bin for convenience. Follow the instructions in the uDW documentation to program it via USB cable.

(Top tip: remove the SD card before programming the firmware as the serial input picks up noise when not connected to a dragon. This can create a number of junk files on the SD card)


### Hardware modification

Locate the three pin header marked 'TAPE' in one corner of the uDW PCB. Pin 1 is nearest to the corner:

- Pin 1 Audio
- Pin 2 Sense
- Pin 3 Ground

A plug is detected when the sense line is connected to ground by the insertion of the plug.

A simple 3.5mm stereo jack socket can be used with the mono plug from the Dragon. The sleeve should be connected to ground, the tip connected to the audio signal, and the ring connected to the plug sense signal. Note that the uDW probably won't detect a stereo plug with this arrangement.

There is only just enough space in the uDW enclosure to fit a socket, and care is required to ensure everything fits together properly. Please refer to the photos as a guide. Note the use of Dupont style connectors and bent header pins to make the cable detachable but the wires can be soldered directly to the PCB if preferred.


### Further information

Original firmware from Tormod's repo here:

https://gitlab.com/tormod/udw-firmware

Tormod's uDW server product page:

http://tormod.me/udw.html

Forum thread:

http://archive.worldofdragon.org/phpBB3/viewtopic.php?f=6&t=4981




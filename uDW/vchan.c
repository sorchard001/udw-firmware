/*
   DriveWire server implementation for uDW
   - DriveWire virtual channels for uDW

   Copyright 2014-2015 Tormod Volden

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stm32f10x_usart.h>
#include "uart.h"
#include "udw.h"
#include "dw.h"
#include "vchan.h"

/* special channel for wifi module support */
#define WIFI_VC 2

/* input buffer to Dragon */
/* statically allocated for now */
#define BUFLEN 256
/* TODO: use a struct for each buffer */
char buf[BUFLEN][VIRTCHANS];
int buflen[VIRTCHANS] = { 0 };
char *wp[VIRTCHANS] = { 0 };
char *rp[VIRTCHANS] = { 0 };
/* output buffer from Dragon */
/* one common for now */
char obuf[BUFLEN];
int owp = 0;

/* from main.c */
void Delay( uint32_t nTime );

static void vc_init_chan(int chan)
{
	if (chan < 0 || chan > VIRTCHANS)
		return;
	if (chan == WIFI_VC) {
		enable_wifi_irq();
		uart_open(WIFI_UART, 115200, 0);
		USART_ClearITPendingBit(WIFI_UART, USART_IT_RXNE);
		USART_ITConfig(WIFI_UART, USART_IT_RXNE, ENABLE);
	}
	/* allocate and reset buffer */
	buflen[chan] = BUFLEN;
	wp[chan] = rp[chan] = &buf[0][chan];
	owp = 0;
}

static void vc_close_chan(int chan)
{
	if (chan < 0 || chan > VIRTCHANS)
		return;
	if (chan == WIFI_VC) {
		USART_ITConfig(WIFI_UART, USART_IT_RXNE, DISABLE);
	}
	buflen[chan] = 0;
	wp[chan] = rp[chan] = 0;
}

/* copy from input buffer into dst buffer */
/* returns number of bytes actually copied */
static int vc_read_ibuf(int chan, char *dst, int len)
{
	int rem = len;

	if (chan < 0 || chan > VIRTCHANS || buflen[chan] == 0)
		return 0;
	while (rem > 0) {
		/* empty buffer? */
		if (wp[chan] == rp[chan]) {
			*dst = '\0';
			return len - rem;
		}
		*dst++ = *rp[chan]++;
		if (rp[chan] == &buf[0][chan] + buflen[chan])
			rp[chan] = &buf[0][chan];
		rem--;
	}
	return len;
}

/* copy into input buffer */
/* returns number of bytes actually copied */
static int vc_write_ibuf(int chan, char *src, int len)
{
	int rem = len;

	if (chan < 0 || chan > VIRTCHANS || buflen[chan] == 0)
		return 0;
	while (rem > 0) {
		/* buffer full? */
		if (wp[chan] + 1 == rp[chan] ||
		    (wp[chan] == &buf[0][chan] + buflen[chan] &&
		     rp[chan] == &buf[0][chan])) {
			return len - rem;
		}
		*wp[chan]++ = *src++;
		if (wp[chan] == &buf[0][chan] + buflen[chan])
			wp[chan] = &buf[0][chan];
		rem--;
	}
	return len;
}

/* used by !VC command */
int vc_in_waiting(int chan)
{
	if (chan < 0 || chan > VIRTCHANS || buflen[chan] == 0)
		return 0;
	if (wp[chan] >= rp[chan])
		return wp[chan] - rp[chan];
	else 
		return buflen[chan] - (rp[chan] - wp[chan]); /* FIXME off by one? */
}

/* uni-directional */
void dw_serinit_op(void)
{
	int chan;

	read_io(&chan);
	vc_init_chan(chan);
}

void dw_serterm_op(void)
{
	int chan;

	read_io(&chan);
	vc_close_chan(chan);
}

void dw_sersetstat_op(void)
{
	int chan;
	int setstatcode;

	read_io(&chan);
	read_io(&setstatcode);
	/* TODO something useful for "device descriptor" */
	if (setstatcode == SS_ComSt) {
		int i;
		int dummy;

		for (i = 0; i < 26 ; i++)
			read_io(&dummy);
	} else if (setstatcode == SS_Open) {
		vc_init_chan(chan);
	} else if (setstatcode == SS_Close) {
		vc_close_chan(chan);
	}
	return;
}

void dw_serread_op(void)
{
	char resp1 = '\0';
	char resp2 = '\0';
	/* byte 1 */
	// int resp_mode = 0;	/* bits 7-6: 00 = VSerial, 10 = VWindow */
	// int come_again = 0;	/* bit 5: always 0 */
	/* option bits 4-0 */
	// int multi = 0;		/* bit 4: determines contents of byte 2 */
	// int chan;		/* bits 3-0 */
	int numb;
	int vc;

	Delay(1);
	/* FIXME wait queue with priorities */
	for (vc = 0; vc < VIRTCHANS; vc++) {
		numb = vc_in_waiting(vc);
		if (numb == 1) {
			resp1 = vc + 1;
			send_io(resp1);
			vc_read_ibuf(vc, &resp2, 1);
			send_io(resp2);
			break; 
		} else if (numb > 1) {
			resp1 = vc + 1 + (1 << 4);	/* set multi bit */
			send_io(resp1);
			send_io(numb);
			break;
		}
	}
	if (!resp1) {
		/* no channels had anything */
		send_io(resp1);
		send_io(resp2);
	}
}

void dw_serreadm_op(void)
{
	int chan;
	int num;
	int i;
	char out;
	int ret;

	read_io(&chan);
	read_io(&num);
	Delay(1);
	for (i = 0; i < num; i++) {
		ret = vc_read_ibuf(chan, &out, 1);
		/* if no more bytes, simply not respond */
		if (ret != 1)
			return;
		send_io(out);
	}
}

void dw_serwrite(int chan)
{
	int out;
	char *reply;

	read_io(&out);

	/* TODO write into multiple output circular buffers */
	obuf[owp] = out;

	/* flush out line on CR */
	if (out == '\r') {
		obuf[owp] = '\0';
		/* put something to keep client finding something */
		reply = "REPLY: ";
		vc_write_ibuf(chan, reply, 7);

		/* virtual channel for wifi module */
		if (chan == WIFI_VC && buflen[WIFI_VC]) {
			print_uart(WIFI_UART, obuf);
			uart_putc('\r', WIFI_UART);
		}
		owp = BUFLEN - 1;
	}
	if (++owp == BUFLEN)
		owp = 0;
}

void dw_serwritem_op(void)
{
	int chan;
	int num;
	int i;

	read_io(&chan);
	read_io(&num);
	for (i = 0; i < num; i++)
		dw_serwrite(chan);
}

#if 0
void poll_wifi(void)
{
	int in;
	char inchar;

	if (buflen[WIFI_VC] == 0)
		return;
	in = uart_getc_timeout(WIFI_UART, 0);
	if (in >= 0) {
		inchar = (char) in;
		vc_write_ibuf(WIFI_VC, &inchar, 1);
	}
}
#endif

/* WIFI module on USART2 */
void USART2_IRQHandler (void)
{
	if (USART_GetITStatus(WIFI_UART, USART_IT_RXNE) != RESET) {
		int in;
		char inchar;

		in = uart_getc(WIFI_UART);
		if (buflen[WIFI_VC] != 0) {
			inchar = (char) in;
			vc_write_ibuf(WIFI_VC, &inchar, 1);
		}
	}
}

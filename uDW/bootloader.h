/*
   DriveWire server implementation for uDW
   - constants shared with bootloader

   Copyright 2014 Tormod Volden

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define MAGIC_RESET_ADDRESS 0x20004E00
#define MAGIC_RESET_APP 0xAFF3AFF3
#define MAGIC_RESET_DFU 0xDF11DF11


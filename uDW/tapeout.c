//
// Dragon/CoCo tape output routines
// Stewart Orchard 2019
//

#include <stdint.h>
#include <string.h>
#include <ff.h>
#include "udw.h"


// milliseconds delay defined elsewhere
void Delay(uint32_t nTime);


int tapeBitCount;
volatile int tapeByteReq;
uint8_t tapeByteCur;
uint8_t tapeByteBuf = 0x55;


// Enable tape audio output stream.
// This will immediately begin outputting bytes.
void tapeoutEnable(void)
{
	tapeBitCount = 0;
	udwEnableTapeTimer();
}

// Stop tape audio output stream.
void tapeoutDisable(void)
{
	while(tapeByteReq < 2){};	// wait for final byte to complete
	udwDisableTapeTimer();
}

// Return next bit to be transmitted
// (called by wave output irq routine)
// Sets a byte request flag when byte buffer empty
int tapeoutNextBit(void)
{
	if(!tapeBitCount) {
		tapeBitCount = 7;
		tapeByteCur = tapeByteBuf;
		tapeByteReq++;
	} else {
		tapeByteCur >>= 1;
		tapeBitCount--;
	}
	return (tapeByteCur & 1);
}

// Wait for byte buffer to become available then write byte
void tapeoutByte(uint8_t byteval)
{
	while(!tapeByteReq){};
	tapeByteReq = 0;
	tapeByteBuf = byteval;
}

// Write a standard length tape leader
void tapeoutLeader(void)
{
	for(int i=0; i<128; i++) {
		tapeoutByte(0x55);
	}
}

// Output standard tape block with checksum
// blocktype: 0=filename, 1=data, 255=EOF
// blocklen: 0-255
void tapeoutBlock(int blocktype, int blocklen, const uint8_t *data)
{
	uint8_t bval, chksum = blocktype + blocklen;

	tapeoutByte(0x55);  // leader
	tapeoutByte(0x3c);  // sync
	tapeoutByte(blocktype);
	tapeoutByte(blocklen);

	for(int i=0; i<blocklen; i++) {
		bval = *data++;
		tapeoutByte(bval);
		chksum += bval;
	}

	tapeoutByte(chksum);
	tapeoutByte(0x55);  // trailer
}


struct TAPE_HEADER {
	uint8_t fname[8];
	uint8_t ftype;
	uint8_t aflag;
	uint8_t gflag;
	uint8_t exec_hi;
	uint8_t exec_lo;
	uint8_t load_hi;
	uint8_t load_lo;
};

struct DDOS_HEADER {
	uint8_t id55;
	uint8_t ftype;
	uint8_t load_hi;
	uint8_t load_lo;
	uint8_t len_hi;
	uint8_t len_lo;
	uint8_t exec_hi;
	uint8_t exec_lo;
	uint8_t idaa;
};


// reads ddos binary and writes out tape file with load & exec address
void tapeoutPlayBoot(void)
{
	FIL fin;
	unsigned int bytesread;
	struct DDOS_HEADER hdr;
	struct TAPE_HEADER thdr;
	uint8_t casbuf[255];

	setRed(255);

	if(f_open(&fin, "tapeboot.bin", FA_READ) != FR_OK) {
		return;
	}
	if(f_read(&fin, &hdr, sizeof(hdr), &bytesread) != FR_OK) {
		f_close(&fin);
		return;
	}
	if(bytesread != sizeof(hdr)) {
		f_close(&fin);
		return;
	}
	if((hdr.id55 != 0x55) & (hdr.ftype != 2) & (hdr.idaa != 0xaa)) {
		f_close(&fin);
		return;
	}

	memcpy(&thdr, "TAPEBOOT", 8);
	thdr.ftype = 0x02;	// binary
	thdr.aflag = 0;		// binary
	thdr.gflag = 0;		// continuous
	thdr.exec_hi = hdr.exec_hi;
	thdr.exec_lo = hdr.exec_lo;
	thdr.load_hi = hdr.load_hi;
	thdr.load_lo = hdr.load_lo;

	tapeoutEnable();
	tapeoutLeader();
	tapeoutBlock(0, sizeof(thdr), (uint8_t *)&thdr);
	tapeoutDisable();

	Delay(500);
	tapeoutEnable();
	tapeoutLeader();

	do {
		if(f_read(&fin, casbuf, 255, &bytesread) != FR_OK) {
			f_close(&fin);
			return;
		}
		tapeoutBlock(1, bytesread, casbuf);
	} while (bytesread == 255);

	tapeoutBlock(0xff, 0, 0);  // EOF block
	tapeoutDisable();
	f_close(&fin);

	setRed(0);
}




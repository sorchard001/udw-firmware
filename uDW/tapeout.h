//
// Dragon/CoCo tape output routines
// Stewart Orchard 2019
//

// Determine value of next bit to write out.
// This is intended to be called by a timer irq routine.
int tapeoutNextBit(void);

// Reads ddos file 'tapeboot.bin' from sd card
// and outputs audio suitable for loading with CLOADM
void tapeoutPlayBoot(void);

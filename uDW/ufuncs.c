/*
   DriveWire server implementation for uDW
   - general purpose functions, stdlib replacements etc

   Copyright 2014 Tormod Volden

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stm32f10x_usart.h>
#include <uart.h>
#include "udw.h"
#include "ufuncs.h"

int ustrncmp(const char *s1, const char *s2, int n)
{
	char p,q;
	while ((p = *s1++) == (q = *s2++) && p && --n);
	return p-q;
}

int ustrcmp(const char *s1, char *s2)
{
	char p,q;
	while ((p = *s1++) == (q = *s2++) && p);
	return p-q;
}

int ustrcpy(char *d, const char *s)
{
	char *o = d;
	while ((*d++ = *s++));
	return d-o-1;
}

unsigned int ustrlen(char *s)
{
	char *o = s;
	while (*s++);
	return s-o;
}

/* TODO: Use the built-in isdigit() from ctype.h instead? */
int uisdigit(const char c)
{
	return c >= '0' && c <= '9';
}

int uscanf2d(const char *start, int *value)
{
	const char *s = start;

	while (*s && !uisdigit(*s))
		s++;
	if (uisdigit(*s))
		*value = *s++ - '0';
	else
		return 0;
	if (uisdigit(*s))
		*value = *value * 10 + *s++ - '0';
	return (s - start);
}

#ifdef UDWDEBUG

void debug_print_2x(int num)
{
	debug_print_char((num>>4)+((num>>4)>9?'A'-10:'0'));
	debug_print_char((num&0xF)+((num&0xF)>9?'A'-10:'0'));
}
	
void debug_print_4x(int num)
{
	debug_print_2x(num >> 8);
	debug_print_2x(num & 0xFF);
}

void debug_print_value(int num)
{
	debug_print_char('$');
	if (num < 0) {
		debug_print("NEG? ");
		return;
	} else if (num > 255) {
		debug_print("OVF? ");
		return;
	}
	debug_print_2x(num);
	debug_print_char(' ');
	return;
}

#endif /* UDWDEBUG */

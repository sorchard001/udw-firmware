/*
   DriveWire server implementation for uDW
   - hardware abstraction routines for uDW

   Copyright 2014-2015 Tormod Volden

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

# include <stm32f10x.h>
# include <stm32f10x_rcc.h>
# include <stm32f10x_gpio.h>
# include <stm32f10x_tim.h>
# include <misc.h>
# include <spi.h>
# include "udw.h"
# include "bootloader.h"
# include "tapeout.h"

void Delay( uint32_t nTime );

void udwResetToDFU(void)
{
	*(volatile uint32_t *)MAGIC_RESET_ADDRESS = MAGIC_RESET_DFU;
	NVIC_SystemReset();
}

int udw_setup_gpio(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;

	// Enable Peripheral Clock for LEDs
	RCC_APB2PeriphClockCmd ( LED_RCC_APB2Periph | RED_RCC_APB2Periph | GREEN_RCC_APB2Periph, ENABLE );

	// Configure Pins for Maple Mini LED
	GPIO_WriteBit (LED_GPIO , LED_Pin , Bit_SET );
	GPIO_StructInit (&GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Pin = LED_Pin ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz ;
	GPIO_Init (LED_GPIO, &GPIO_InitStructure );

	// Configure uDW red/green LED pins for PWM
	GPIO_StructInit (&GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Pin = RED_Pin | GREEN_Pin ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz ;
	GPIO_Init (RED_GPIO, &GPIO_InitStructure );

	/* Set up timer for PWM controlled LEDs */
	RCC_APB1PeriphClockCmd ( RCC_APB1Periph_TIM4, ENABLE );
	TIM_TimeBaseStructure.TIM_Period = 255 - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = 1800 - 1; /* 40 kHz */
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
	TIM_Cmd(TIM4, ENABLE);

	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_Pulse = 255; /* not reached */
	/* red LED is on timer 4 channel 1 */
	TIM_OC1Init(TIM4, &TIM_OCInitStructure);
	/* green LED is on timer 4 channel 2 */
	TIM_OC2Init(TIM4, &TIM_OCInitStructure);

	/* Configure SD power control pin */
	RCC_APB2PeriphClockCmd ( SD_PWR_APB2Periph , ENABLE );
	/* preset output value to avoid glitches */
	powerSD(1);
	GPIO_StructInit (&GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Pin = SD_PWR_Pin ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz ;
	GPIO_Init (SD_PWR_GPIO, &GPIO_InitStructure );

	/* Configure SD Slave Select pin */
	RCC_APB2PeriphClockCmd ( SD_NSS_APB2Periph , ENABLE );
	/* start low, SPI init will set it high */
	/* should be held low while SD card is unpowered*/
	GPIO_WriteBit (SD_NSS_GPIO , SD_NSS_Pin , Bit_RESET );
	GPIO_StructInit (&GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Pin = SD_NSS_Pin ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz ;
	GPIO_Init (SD_NSS_GPIO, &GPIO_InitStructure );

	/* Configure input pin for SD card detect */
	RCC_APB2PeriphClockCmd ( SCD_RCC_APB2Periph , ENABLE );
	GPIO_StructInit (&GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Pin = SCD_Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz ;
	GPIO_Init (SCD_GPIO, &GPIO_InitStructure );

	/* Configure sound/tape output pin */
	RCC_APB2PeriphClockCmd ( SND_RCC_APB2Periph , ENABLE );
	snd_out(0);
	GPIO_StructInit (&GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Pin = SND_Pin ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz ;
	GPIO_Init (SND_GPIO, &GPIO_InitStructure );

	/* Set up timer 2 for tape output */
	RCC_APB1PeriphClockCmd ( RCC_APB1Periph_TIM2, ENABLE );
	TIM_TimeBaseStructure.TIM_Prescaler = 1000 - 1;
	TIM_TimeBaseStructure.TIM_Period = 15 - 1; // 4800 Hz
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

	/* Configure sound/tape jack detect */
	RCC_APB2PeriphClockCmd ( JACK_RCC_APB2Periph , ENABLE );
	GPIO_StructInit (&GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Pin = JACK_Pin ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz ;
	GPIO_Init (JACK_GPIO, &GPIO_InitStructure );

	/* Configure WIFI power down control pin */
	RCC_APB2PeriphClockCmd ( WFPD_RCC_APB2Periph , ENABLE );
	/* preset output value to avoid glitches */
	powerWIFI(0);
	GPIO_StructInit (&GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Pin = WFPD_Pin ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz ;
	GPIO_Init (WFPD_GPIO, &GPIO_InitStructure );

	return 0;
}

void setRed(int intensity)
{
	TIM4->CCR1 = 255 - (intensity & 0xFF);
}

void setGreen(int intensity)
{
	TIM4->CCR2 = 255 - (intensity & 0xFF);
}

void powerSD(int enable)
{
	GPIO_WriteBit (SD_PWR_GPIO , SD_PWR_Pin , enable?Bit_RESET:Bit_SET );
}

void powerDownSD()
{
	spiDeInit(SD_SPI);
	// MOSI, MISO, SCK pulled down in spiDeInit() */
	GPIO_WriteBit (SD_NSS_GPIO , SD_NSS_Pin , Bit_RESET );
	powerSD(0);
}

void powerUpSD()
{
	powerSD(1);
	Delay(100);
	spiInit(SD_SPI);
}

/* read SD card detect status */
int udwCardDetect(void)
{
	return GPIO_ReadInputDataBit(SCD_GPIO, SCD_Pin) == 0;
}

/* needed to liberate PA14 (and PA13 PA15 PB3 PB4) */
void disableJTAG(void)
{
	RCC_APB2PeriphClockCmd ( RCC_APB2Periph_AFIO, ENABLE );
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable, ENABLE);
}

void snd_out(int value)
{
	/* TODO use PWM */
	GPIO_WriteBit (SND_GPIO , SND_Pin , value?Bit_SET:Bit_RESET );
}

/* Detect tape jack */
int udwJackDetect(void)
{
	return GPIO_ReadInputDataBit(JACK_GPIO, JACK_Pin) == 0;
}

void udwEnableTapeTimer(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	TIM2->CNT = 0;
	TIM_Cmd(TIM2, ENABLE);
}

void udwDisableTapeTimer(void)
{
	TIM_Cmd(TIM2, DISABLE);
	TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
}

/* Toggle tape wave out on each timer irq
 * Set up next interval on rising edge of wave */
void TIM2_IRQHandler(void)
{
	static int sndlev = 0;

	// clear interrupt flag early to ensure delayed write
	// makes it to the hardware before ISR exit
	// (Otherwise the interrupt may get called twice)
	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);

	sndlev = 255 - sndlev;
	snd_out(sndlev);

	if(sndlev) {
		TIM2->ARR = (tapeoutNextBit()) ? 15-1 : 30-1;
	}
}

void snd_test(int duration)
{
	int rep = duration;

	setRed(255);
	while (rep--) {
		snd_out(255);
		Delay(1);
		snd_out(0);
		Delay(1);
	}
	setRed(0);
}

/* FIXME use PWM */
void hf_test(int duration)
{
	int rep = duration * 1000;

	setRed(255);
	while (rep--) {
		snd_out(255);
		// Delay(1);
		snd_out(0);
		// Delay(1);
	}
	setRed(0);
}

void powerWIFI(int enable)
{
	GPIO_WriteBit (WFPD_GPIO , WFPD_Pin , enable?Bit_SET:Bit_RESET );
}

void enable_wifi_irq(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Enable the USART2 interrupt for reading from wifi module */
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

/*
   DriveWire server implementation for uDW
   - uDW specific RTC and time functions

   Copyright 2014 Tormod Volden

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

int rtc_running();
int setup_time(int reset);
void rtc_start_crystal();
void rtc_finish();
void setdate(const char *timestr);
void settime(const char *timestr);

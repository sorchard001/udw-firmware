/*
   DriveWire server implementation for uDW
   - debug functionality for uDW
   (not needed for DriveWire operation)

   Copyright 2014-2015 Tormod Volden

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stm32f10x.h>
#include <uart.h>
#include <ff.h>
#include "ufuncs.h"
#include "dw.h"
#include "udw.h"
#include "udw_debug.h"
#include "rtc_32f1.h"
#include "rtc_udw.h"

// # include "console9341.h"
// #define console_print(...) print_uart(DEBUG_UART, __VA_ARGS__)
// #define console_print_char(...) uart_putc(__VA_ARGS__, DEBUG_UART)

#ifdef UDWDEBUG

/* from main.c */
extern __IO uint32_t TimingDelay ;
extern __IO uint32_t Timer ;
void Delay( uint32_t nTime );
extern USART_TypeDef *active_uart;
extern int debug_cursor_enabled;

/* from dw.c */
extern FIL Fil;
extern char Buff[1024];
extern int debug_stat;
extern int startuptime, firstreqtime;
extern int opentime, seektime, readtime;

/* private */
static char key_buff[16];
static int key_i;

/* private prototypes */
static void debugInterpret(char *inputline);
static int debugPrintFile(const char *fname);

void debugInput(char in)
{
		if (in == '\r') {
			key_buff[key_i] = 0;
			debugInterpret(key_buff);
			key_i = 0;
			// console_erase_eol();
		} else if (in == '\b') {
			if (key_i > 0) {
				key_i--;
				debug_print_char('\b');
			}
		} else if (in == '\f') {
			// console_init();
		} else if (in > 31 && in < 128) {
			key_buff[key_i] = in;
			debug_print_char(in);
			if (key_i < sizeof(key_buff))
				key_i++;
		} else {
			debug_print_value(in);
			// debug_print(" was received\n");
		}
}


static int debugPrintFile(const char *fname)
{
	FRESULT rc;

	debug_print("Opening the file ");
	debug_print(fname);
	debug_print("\n");
	rc = f_open(&Fil, fname, FA_READ);
	if (rc != FR_OK) {
		debug_print("Bad filename\n");
		return 1;
	} else {
		int bytesRead;
		int i;

		debug_print("File contents:\n");
		rc = f_read(&Fil, Buff, sizeof(Buff), &bytesRead);
		if (rc || !bytesRead) bytesRead = 0;
		for (i = 0; i < bytesRead; i++)
			debug_print_char(Buff[i]);

		debug_print("\nClosing the file.\n");
		rc = f_close(&Fil);
	}
	return 0;
}

/* debug commands - usually from debug port */
static void debugInterpret(char *inputline)
{
	FRESULT rc;

	debug_print("\n");
	if (!inputline[0])
		return;
	if (!ustrncmp(inputline, "ls", 2)) {
		debug_print("listing directory\n");
		if(inputline[2] == ' ')
			inputline++;
		if(list_dir(inputline + 2))
			debug_print(" list_dir failed");
		debug_print("\n\r");
	} else if (!ustrcmp(inputline, "q")) {
		debug_print("Listening on dw port\n");
		active_uart = IO_UART;
		debug_cursor_enabled = 0;
	} else if (!ustrcmp(inputline, "ili")) {
		uint32_t left;
		TimingDelay = 0xFFFF;
		// LCD_Color_Bars_Demo();
		left = TimingDelay;
		debug_print_char('$');
		debug_print_4x(0xFFFF - left);
		debug_print(" ms\n");
	} else if (!ustrcmp(inputline, "loop")) {
		int c;
		uint32_t left;
		TimingDelay = 0xFFFF;
		for (c = 0; c<10000000; c++)
			asm("nop");
		left = TimingDelay;
		debug_print_char('$');
		debug_print_4x(0xFFFF - left);
		debug_print(" ms\n");
	} else if (!ustrcmp(inputline, "kls")) {
		// console_init();
	} else if (!ustrcmp(inputline, "kursor")) {
		debug_cursor_enabled = 1 - debug_cursor_enabled;
	} else if (inputline[0] == '|' ) {
		const char *i;
		int in;

		for (i = inputline + 1; *i; i++) {
			uart_putc(*i, IO_UART);
			in = uart_getc_timeout(IO_UART, READ_TIMEOUT);
			if (in > 0)
				debug_print_char((char) in);
		}
		do {
			in = uart_getc_timeout(IO_UART, READ_TIMEOUT);
			if (in > 0)
				debug_print_char((char) in);
		} while (in > 0);
		debug_print_char('\n');
	} else if (!ustrcmp(inputline, "gpio")) {
		debug_print("\rGPIOA: ");
		debug_print_4x(GPIOA->CRH >> 16);
		debug_print_4x(GPIOA->CRH & 0xFFFF);
		debug_print_4x(GPIOA->CRL >> 16);
		debug_print_4x(GPIOA->CRL & 0xFFFF);
		debug_print(" Value: ");
		debug_print_4x(GPIOA->IDR & 0xFFFF);
		debug_print("\n\rGPIOB: ");
		debug_print_4x(GPIOB->CRH >> 16);
		debug_print_4x(GPIOB->CRH & 0xFFFF);
		debug_print_4x(GPIOB->CRL >> 16);
		debug_print_4x(GPIOB->CRL & 0xFFFF);
		debug_print(" Value: ");
		debug_print_4x(GPIOB->IDR & 0xFFFF);
		debug_print("\n\rGPIOC: ");
		debug_print_4x(GPIOC->CRH >> 16);
		debug_print_4x(GPIOC->CRH & 0xFFFF);
		debug_print_4x(GPIOC->CRL >> 16);
		debug_print_4x(GPIOC->CRL & 0xFFFF);
		debug_print(" Value: ");
		debug_print_4x(GPIOC->IDR & 0xFFFF);
	} else if (!ustrcmp(inputline, "time")) {
		RTCTIME rtc;
		rtc_gettime (&rtc);
		debug_print_value(rtc.hour);
		debug_print_value(rtc.min);
		debug_print_value(rtc.sec);
	} else if (!ustrncmp(inputline, "setTime", 7)) {
		settime(inputline + 7);
	} else if (!ustrncmp(inputline, "setDate", 7)) {
		setdate(inputline + 7);
	} else {
		FILINFO fno;

		rc = f_stat(inputline, &fno);
		if (rc != FR_OK) {
			debug_print("error\n");
			return;
		} else if (fno.fattrib & AM_DIR) {
			f_chdir(inputline);
		} else {
			debugPrintFile(inputline);
		}
	}
}


void debugStats(int error_condition, int lsn)
{
	/* diagnostics on debug port */
	if (error_condition)
		debug_print_value(error_condition);
	else if (debug_stat && lsn == 0) {
		debug_print_char('#');
		debug_print_4x(opentime);
		debug_print_char('-');
		debug_print_4x(seektime);
		debug_print_char('-');
		debug_print_4x(readtime);
		debug_print_char(';');
		debug_print_4x(startuptime);
		debug_print_char(':');
		debug_print_4x(firstreqtime);
	} else if (debug_stat) {
		debug_print_char('#');
	}

#if 0
	debug_print_value(dnum);
	debug_print_value(lsn0);
	debug_print_value(lsn1);
	debug_print_value(lsn2);
	debug_print("- ");

	debug_print_2x(chk1);
	debug_print_2x(chk2);
	debug_print(" = ");
	debug_print_4x(checksum);
	debug_print("#\n");
	return 0;
#endif
}

#endif /* UDWDEBUG */
